# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Student, Attendance

# Register your models here.


class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'father_name', 'batch_no')


class AttendanceAdmin(admin.ModelAdmin):
    list_display = ('user', 'date', 'attendance_status')

admin.site.register(Student, StudentAdmin)
admin.site.register(Attendance, AttendanceAdmin)