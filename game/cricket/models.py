# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.


class Student(models.Model):
    name = models.CharField(max_length=20)
    father_name = models.CharField(max_length=20)
    batch_no = models.IntegerField(default=0)


class Attendance(models.Model):
    user = models.ForeignKey(Student)
    date = models.DateField()
    attendance_status = models.BooleanField(default=False)