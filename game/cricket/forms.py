from django import forms


class LoginForm(forms.Form):

    user_name = forms.CharField(max_length=10)
    password = forms.CharField(widget=forms.PasswordInput())