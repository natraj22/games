from django.conf.urls import url
from .views import MainPageView, AttendanceListView, StudentInfoView
from django.contrib.auth.views import login


urlpatterns = [
    url(r'^user/$', MainPageView.as_view()),
    url(r'^login/$', login, {'template_name': 'registration/login.html'}),
    url(r'^user/(?P<id>[0-9]+)/$', AttendanceListView.as_view()),
    url(r'^coach/user_info/(?P<id>[0-9]+)/$', StudentInfoView.as_view()),
]