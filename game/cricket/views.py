# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render
from django.views.generic import TemplateView, FormView
from django.contrib.auth.models import User

from django.contrib.auth import authenticate
from django.http import *
from .models import Student, Attendance
from .serializers import AttendanceSerializer

from cricket.forms import LoginForm


class MainPageView(TemplateView):
    template_name = 'admin/home.html'
    form_class = LoginForm

    def get_context_data(self, **kwargs):
        context = super(MainPageView, self).get_context_data(**kwargs)
        context['admin'] = self.request.user.is_superuser
        # context['user_attendance'] = Attendance.objects.filter(user_id=self.request.user.id)

        return context

    def post(self, request):
        id = self.request.POST['id']
        try:
            user = Student.objects.get(id=id)
        except User.DoesNotExists:
            return HttpResponse(status=404)
        if not self.request.user.is_staff:
            return HttpResponseRedirect('/auth/user/{id}'.format(id=user.id))
        else:
            return HttpResponseRedirect('/auth/coach/user_info/{id}'.format(id=user.id))


class StudentInfoView(TemplateView):
    template_name = 'admin/student_info.html'

    def get_context_data(self, **kwargs):
        context = super(StudentInfoView, self).get_context_data(**kwargs)
        context['user_attendance'] = Attendance.objects.filter(user_id=self.kwargs['id'])
        context['is_coach'] = self.request.user.is_staff
        return context


class AttendanceListView(APIView):

    def get(self, request, id):
        try:
            attendance = Attendance.objects.filter(user_id=id)
        except Attendance.DoesNotExists:
            return HttpResponse(status=404)
        serializer = AttendanceSerializer(attendance, many=True)
        return Response(serializer.data)
