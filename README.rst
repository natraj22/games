Games project
================

Prerequisites
-------------

1. Create a virtual environment for the project to work on
     virtualenv games

2. Activate virtualenv
     source games/bin/activate

3. Get all the source code from gitLab into the local system
     git clone  https://gitlab.com/natraj22/games.git




Installation (environment setup)
-------------------------------
1. cd games

2. pip install -r requirement.txt

3. Go to settings.py file and modify database credentials(follow below 'Database' steps too).

4. cd games/game

5. python manage.py makemigrations

6. python manage.py migrate

7. python manage.py createsuperuser




Database
--------
open mysql
  mysql -u root -p

create database with below command
  create database games


Run Application
----------------


python manage.py runserver 8000

go to below url and login with created super user

http://127.0.0.1:8000/auth/login/

go to admin view with below url

http://127.0.0.1:8000/admin
